Diplonat
========

## Feature set

 * [X] (Re)Configure NAT via UPNP/IGD (prio: high)  
 * [X] (Re)Configure iptables (prio: low)  
 * [ ] (Re)Configure DNS via ??? (prio: low)  

## Understand scope

 * Reconfigure __local__ environment when provisionning a __cluster__ service
   * Reconfigure host on demand according to service needs (Firewall)
   * Reconfigure host local network according to service needs (Router NAT)
 * Operate a global reconfiguration that associate the tuple (__local__ environment information, a __cluster__ service)
   * Reconfigure an external service with local info (DNS with public IP returned by the router via IGD)

## Dependencies

The `reqwest` crate "will make use of system-native transport layer security to connect to HTTPS destinations". See [`reqwest`'s documentation](https://docs.rs/reqwest/0.9.18/reqwest/#tls) for more information. 


## Operate

You need to add the following to your nomad config file :

```
client {
  [...]

  options {
    docker.privileged.enabled = "true"
  } 
}
```


```bash
cargo build
consul agent -dev # in a separate terminal

# adapt following values to your configuration
export DIPLONAT_CONSUL_NODE_NAME="lheureduthe"
export DIPLONAT_FIREWALL_ENABLE="true"
export DIPLONAT_FIREWALL_REFRESH_TIME="300"
export DIPLONAT_IGD_ENABLE="true"
export DIPLONAT_IGD_PRIVATE_IP="192.168.0.18"
export DIPLONAT_IGD_REFRESH_TIME="60"
export DIPLONAT_IGD_EXPIRATION_TIME="300"
export RUST_LOG=debug
cargo run
```

## Design Guidelines

Diplonat is made of a set of Components.
Components communicate between them thanks to [tokio::sync::watch](https://docs.rs/tokio/0.2.21/tokio/sync/index.html#watch-channel) transferring copiable messages.
Each message must contain the whole state (and not a transition) as messages can be lost if a more recent message is received.
This choice has been made to limit bugs.
If you need to watch two actors and merge their content, you may use [tokio::sync::select](https://docs.rs/tokio/0.2.21/tokio/macro.select.html).
When you read a value from source 1, you must cache it to be able to merge it later when you read from source 2.

## About Consul Catalog

  * We query the `/v1/catalog/node/<node>` endpoint
  * We can watch it thanks to [Blocking Queries](https://www.consul.io/api/features/blocking.html)

eg:

```bash
curl -vvv http://127.0.0.1:8500/v1/catalog/node/lheureduthe
# returns X-Consul-Index: 15
curl -vvv http://127.0.0.1:8500/v1/catalog/node/lheureduthe?index=15
```

Each time you do the request, the whole list of services bound to the node is returned.


To test the Consul Catalog part, you can do:

```bash
consul agent -dev #in a separate terminal, if not already running
consul services register -name=fake_leet -tag="(diplonat (tcp_port 1337) (tcp_port 1338 1339))"
consul services register -name=fake_dns  -tag="(diplonat (udp_port 53) (tcp_port 53))"
consul services register -name=fake_irc  -tag="(diplonat (udp_port 6667 6666))"
consul services -id=example
```
