mod config;
mod consul;
mod consul_actor;
mod diplonat;
mod fw;
mod fw_actor;
mod igd_actor;
mod messages;

use log::*;
use diplonat::Diplonat;

#[tokio::main]
async fn main() {
  pretty_env_logger::init();
  info!("Starting Diplonat");

  let mut diplo = Diplonat::new().await.expect("Setup failed");
  diplo.listen().await.expect("A runtime error occured");
}
