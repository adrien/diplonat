use std::collections::HashMap;
use std::time::Duration;

use crate::config::*;

// Environment variables are set for the entire process and
// tests are run whithin the same process.
// => We cannot test ConfigOpts::from_env(), 
// because tests modify each other's environment.
// This is why we only test ConfigOpts::from_iter(iter).

fn minimal_valid_options() -> HashMap<String, String> {
	let mut opts = HashMap::new();
    opts.insert("DIPLONAT_CONSUL_NODE_NAME".to_string(), "consul_node".to_string());
    opts
}

fn all_valid_options() -> HashMap<String, String> {
	let mut opts = minimal_valid_options();
    opts.insert("DIPLONAT_CONSUL_URL".to_string(), "http://127.0.0.1:9999".to_string());
	opts.insert("DIPLONAT_ACME_ENABLE".to_string(), "true".to_string());
	opts.insert("DIPLONAT_ACME_EMAIL".to_string(), "bozo@bozo.net".to_string());
	opts.insert("DIPLONAT_FIREWALL_ENABLE".to_string(), "true".to_string());
	opts.insert("DIPLONAT_FIREWALL_REFRESH_TIME".to_string(), "20".to_string());
	opts.insert("DIPLONAT_IGD_ENABLE".to_string(), "true".to_string());
	opts.insert("DIPLONAT_IGD_PRIVATE_IP".to_string(), "172.123.43.555".to_string());
	opts.insert("DIPLONAT_IGD_EXPIRATION_TIME".to_string(), "60".to_string());
	opts.insert("DIPLONAT_IGD_REFRESH_TIME".to_string(), "10".to_string());
    opts
}

// #[test]
// #[should_panic]
// fn err_empty_env() {
// 	 std::env::remove_var("DIPLONAT_CONSUL_NODE_NAME");
// 	 ConfigOpts::from_env().unwrap();
// }

#[test]
#[should_panic]
fn err_empty_env() {
	std::env::remove_var("DIPLONAT_CONSUL_NODE_NAME");
	let opts: HashMap<String, String> = HashMap::new();
	ConfigOpts::from_iter(opts).unwrap();
}

#[test]
fn ok_minimal_valid_options() {
	let opts = minimal_valid_options();
	let rt_config = ConfigOpts::from_iter(opts.clone()).unwrap();

	assert_eq!(
		&rt_config.consul.node_name, 
		opts.get(&"DIPLONAT_CONSUL_NODE_NAME".to_string()).unwrap()
	);
	assert_eq!(
		rt_config.consul.url, 
		CONSUL_URL.to_string()
	);
	assert!(rt_config.acme.is_none());
	assert!(rt_config.firewall.is_none());
	assert!(rt_config.igd.is_none());
	/*assert_eq!(
		rt_config.firewall.refresh_time,
		Duration::from_secs(REFRESH_TIME.into())
	);
	assert_eq!(
		&rt_config.igd.private_ip,
		opts.get(&"DIPLONAT_PRIVATE_IP".to_string()).unwrap()
	);
	assert_eq!(
		rt_config.igd.expiration_time,
		Duration::from_secs(EXPIRATION_TIME.into())
	);
	assert_eq!(
		rt_config.igd.refresh_time,
		Duration::from_secs(REFRESH_TIME.into())
	);*/
}

#[test]
#[should_panic]
fn err_invalid_igd_options() {
	let mut opts = minimal_valid_options();
	opts.insert("DIPLONAT_IGD_ENABLE".to_string(), "true".to_string());
	opts.insert("DIPLONAT_IGD_EXPIRATION_TIME".to_string(), "60".to_string());
	opts.insert("DIPLONAT_IGD_REFRESH_TIME".to_string(), "60".to_string());
	ConfigOpts::from_iter(opts).unwrap();
}

#[test]
fn ok_all_valid_options() {
	let opts = all_valid_options();
	let rt_config = ConfigOpts::from_iter(opts.clone()).unwrap();

	let firewall_refresh_time = Duration::from_secs(
		opts.get(&"DIPLONAT_FIREWALL_REFRESH_TIME".to_string()).unwrap()
			.parse::<u64>().unwrap()
			.into());
	let igd_expiration_time = Duration::from_secs(
		opts.get(&"DIPLONAT_IGD_EXPIRATION_TIME".to_string()).unwrap()
			.parse::<u64>().unwrap()
			.into());
	let igd_refresh_time = Duration::from_secs(
		opts.get(&"DIPLONAT_IGD_REFRESH_TIME".to_string()).unwrap()
			.parse::<u64>().unwrap()
			.into());

	assert_eq!(
		&rt_config.consul.node_name, 
		opts.get(&"DIPLONAT_CONSUL_NODE_NAME".to_string()).unwrap()
	);
	assert_eq!(
		&rt_config.consul.url, 
		opts.get(&"DIPLONAT_CONSUL_URL".to_string()).unwrap()
	);

	assert!(rt_config.acme.is_some());
	let acme = rt_config.acme.unwrap();
	assert_eq!(
		&acme.email,
		opts.get(&"DIPLONAT_ACME_EMAIL".to_string()).unwrap());

	assert!(rt_config.firewall.is_some());
	let firewall = rt_config.firewall.unwrap();
	assert_eq!(
		firewall.refresh_time,
		firewall_refresh_time
	);

	assert!(rt_config.igd.is_some());
	let igd = rt_config.igd.unwrap();
	assert_eq!(
		&igd.private_ip,
		opts.get(&"DIPLONAT_IGD_PRIVATE_IP".to_string()).unwrap()
	);
	assert_eq!(
		igd.expiration_time,
		igd_expiration_time
	);
	assert_eq!(
		igd.refresh_time,
		igd_refresh_time
	);
}