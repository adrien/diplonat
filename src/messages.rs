use std::collections::HashSet;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PublicExposedPorts {
  pub tcp_ports: HashSet<u16>,
  pub udp_ports: HashSet<u16>
}

impl PublicExposedPorts {
  pub fn new() -> Self {
    return Self {
      tcp_ports: HashSet::new(),
      udp_ports: HashSet::new()
    }
  }
}
